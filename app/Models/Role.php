<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends Model
{
    use HasFactory, LogsActivity;
    protected $table = 'abilities';
    protected $fillable = ['role_name'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
