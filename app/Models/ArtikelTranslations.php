<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArtikelTranslations extends Model
{
    use HasFactory;

    protected $table = 'artikel_translations';
}
