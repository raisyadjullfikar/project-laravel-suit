<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RichanFongdasen\I18n\Contracts\TranslatableModel;
use RichanFongdasen\I18n\Eloquent\Concerns\Translatable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Artikel extends Model implements TranslatableModel, HasMedia
// class Artikel extends Model
{
    use Translatable, HasFactory, LogsActivity, InteractsWithMedia, HasSlug;
    protected $table = 'artikel';
    protected $fillable = ['judul', 'content', 'image_content', 'category', 'user_id'];
    protected array $translates = [
        'slug',
    ];
    protected string $translationTable = 'artikel_translations';
    protected $hidden = ['translations'];


    /**
     * Get the translated `slug` attribute.
     *
     * @return string
     * @throws \ErrorException
     */
    public function getSlugAttribute(): string
    {
        return (string) $this->getAttribute('slug');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['judul', 'content', 'image_content', 'category']);
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('judul')
            ->saveSlugsTo('sluggable');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'sluggable';
    }

    // public function user(): BelongsTo
    // {
    //     return $this->belongsTo(User::class, 'user_id');
    // }
}
