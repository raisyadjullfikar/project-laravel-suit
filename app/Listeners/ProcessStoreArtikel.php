<?php

namespace App\Listeners;

use App\Events\StoreArtikelProcess;
use App\Jobs\StoreArtikelJob;

class ProcessStoreArtikel
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(StoreArtikelProcess $event): void
    {
        StoreArtikelJob::dispatch($event->artikel);
    }
}
