<?php

namespace App\Exports;

use App\Models\Artikel;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Spatie\QueryBuilder\QueryBuilder;

class ExportArtikel implements FromCollection, WithHeadings
{
    public bool $isAdmin;
    public $id;
    public function  __construct($isAdmin, $id)
    {
        $this->isAdmin= $isAdmin;
        $this->id = $id;
    }

    public function headings(): array
    {
        return [
            'id',
            'Judul',
            'Content',
            'Gambar',
            'Kategori',
            'Slug',
            'Tanggal Submit',
            'Tanggal Publikasi',
            'User Id',
            'Created At',
            'Updated At'
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // dd($this->isAdmin . ' - ' . $this->id);
        if ($this->isAdmin)
        {
            return QueryBuilder::for(Artikel::class)
            ->allowedFields(['id', 'judul', 'content', 'category', 'sluggable', 'tanggal_submit', 'tanggal_publikasi', 'user_id'])
            ->defaultSort('judul')
            ->get();
        } else
        {
            return QueryBuilder::for(Artikel::class)
                ->allowedFields(['id', 'judul', 'content', 'category', 'sluggable', 'tanggal_submit', 'tanggal_publikasi', 'user_id'])
                ->defaultSort('judul')
                ->where('user_id', $this->id)
                ->get();
        }
        // return QueryBuilder::for(Artikel::class)
        //     ->allowedFields(['id', 'judul', 'content', 'category', 'sluggable', 'tanggal_submit', 'tanggal_publikasi', 'user_id'])
        //     ->defaultSort('judul')
        //     ->get();
        // $user = auth('sanctum')->user();
        // $userSign = User::find($user->id);
        // $roles = $userSign->getRoleNames();
        // $isUserAdmin = in_array('admin', $roles->toArray());
        // if ($isUserAdmin)
        // {
        //     return QueryBuilder::for(Artikel::class)
        //     ->allowedFields(['id', 'judul', 'content', 'category', 'sluggable', 'tanggal_submit', 'tanggal_publikasi', 'user_id'])
        //     ->defaultSort('judul')
        //     ->get();
        // } else 
        // {
        //     return QueryBuilder::for(Artikel::class)
        //     ->allowedFields(['id', 'judul', 'content', 'category', 'sluggable', 'tanggal_submit', 'tanggal_publikasi', 'user_id'])
        //     ->defaultSort('judul')
        //     ->where('user_id', $user->id)
        //     ->get();
        // }
    }
}
