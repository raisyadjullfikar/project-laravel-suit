<?php

namespace App\Events;

use App\Jobs\StoreArtikelJob;
use App\Models\Artikel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreArtikelProcess
{
    use Dispatchable, InteractsWithSockets, SerializesModels, InteractsWithQueue;

    public $artikel;
    /**
     * Create a new event instance.
     */
    public function __construct(Artikel $artikel)
    {
        $this->artikel = $artikel;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('channel-name'),
        ];
    }
}
