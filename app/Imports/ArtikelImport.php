<?php

namespace App\Imports;

use App\Models\Artikel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;

class ArtikelImport implements ToCollection
{
    // /**
    // * @param array $row
    // *
    // * @return \Illuminate\Database\Eloquent\Model|null
    // */
    // public function model(array $row)
    // {
    //     return new Artikel([
    //         'judul'     => $row[0],
    //         'content'    => $row[1], 
    //         'image_content'    => $row[2], 
    //         'category'    => $row[3], 
    //         // 'sluggable'    => $row[4], 
    //         // 'tanggal_submit'    => $row[5], 
    //         // 'tanggal_publikasi'    => $row[6], 
    //         'user_id'    => $row[4],
    //         // 'password' => Hash::make($row[2]),
    //     ]);
    // }

    public function collection(Collection $rows)
    {
        // dd($rows);
        foreach ($rows as $row) 
        {
            Artikel::create([
                'judul'     => $row[0],
                'content'    => $row[1], 
                'image_content'    => $row[2], 
                'category'    => $row[3], 
                'user_id'    => $row[4],
            ]);
        }
    }
}
