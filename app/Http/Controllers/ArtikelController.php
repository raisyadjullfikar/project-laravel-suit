<?php

namespace App\Http\Controllers;

use App\Exports\ExportArtikel;
use App\Http\Controllers\Api\ArtikelController as ApiArtikelController;
use App\Imports\ArtikelImport;
use App\Models\Artikel;
use App\Models\User;
use App\Services\HttpService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Laravel\Sanctum\PersonalAccessToken;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Spatie\QueryBuilder\QueryBuilder;

class ArtikelController extends Controller
{
    protected $httpService;

    public function __construct(HttpService $httpService)
    {
        $this->httpService = $httpService;
    }

    public function accept_artikel($ids)
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $bearerNon = session('bearer_token_non_encrypt');
            [$id, $token] = explode('|', $bearerNon, 2);
            $url = "http://project-real-1.localhost/api/artikel-accept/{$ids}";
            $data = [
                'tanggal_publikasi' => now(),
            ];
            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ];
            $check = $this->httpService->put($url, $data, $headers);
            if ($check['status']) {
                Session::flash('message_publication_accepted', "Berhasil mempublikasikan artikel user");
                return redirect('/artikel-non-publication');
            } else return response()->json([
                'status' => $check
            ]);
        } else {
            return redirect('login');
        }
    }

    public function home(): View|Response|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $artikelHomeController = new ApiArtikelController();
            $responses = $artikelHomeController->home();
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $url = 'http://project-real-1.localhost/api/home';
                $headers = [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ];
                $check = $this->httpService->get($url, $headers);

                if ($check->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    $paths = [];
                    foreach ($data['data'] as $artikel) {
                        $datatest = Artikel::find((int)$artikel['id']);

                        // Memeriksa apakah artikel memiliki media terkait
                        if ($datatest->getMedia('images')->isNotEmpty()) {
                            $link = $datatest->getMedia('images')->first()->getUrl();
                            $url_parts = parse_url($link);
                            $paths[$artikel['id']] = $url_parts['path'];
                        } else {
                            $paths[$artikel['id']] = ''; // Contoh menggunakan nilai kosong
                        }
                    }

                    foreach ($data['data'] as &$artikel) {
                        // dd($paths[$artikel['id']]);
                        // Memeriksa apakah id artikel terdapat dalam $paths
                        if (isset($paths[$artikel['id']])) {
                            $artikel['image_content'] = $paths[$artikel['id']];
                        } else {
                            // Tindakan yang akan diambil jika tidak ada path yang ditemukan untuk id artikel
                            $artikel['image_content'] = ''; // Atau nilai default lainnya
                        }
                    }
                    // dd($data['data'][0]);
                    return view('Pages.Home', ['data' => $data['data']]);
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ]);
            } else if ($responses->getStatusCode() == 401) {
                return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized | Anda Belum Login'
                ], 401);
            }
        } else {
            return redirect('login');
        }
    }

    public function index(Request $request): View|Response|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $artikelHomeController = new ApiArtikelController();
            $responses = $artikelHomeController->index($request);
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $url = 'http://project-real-1.localhost/api/artikel';
                $headers = [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ];
                $check = $this->httpService->get($url, $headers);

                if ($check->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    $paths = [];
                    foreach ($data['data']['data'] as $artikel) {
                        // dd($data['data']['data']);
                        $datatest = Artikel::find((int)$artikel['id']);

                        if ($datatest->getMedia('images')->isNotEmpty()) {
                            $link = $datatest->getMedia('images')->first()->getUrl();
                            $url_parts = parse_url($link);
                            $paths[$artikel['id']] = $url_parts['path'];
                        } else {
                            $paths[$artikel['id']] = '';
                        }
                    }

                    foreach ($data['data']['data'] as &$artikel) {
                        if (isset($paths[$artikel['id']])) {
                            $artikel['image_content'] = $paths[$artikel['id']];
                        } else {
                            $artikel['image_content'] = '';
                        }
                    }
                    return view('Pages.Artikel', ['data' => $data]);
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ]);
            } else if ($responses->getStatusCode() == 401) {
                return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized | Anda Belum Login'
                ], 401);
            }
        } else {
            return redirect('login');
        }
    }

    public function myArtikel(): View|Response|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $artikelHomeController = new ApiArtikelController();
            $responses = $artikelHomeController->myArtikel();
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $url = 'http://project-real-1.localhost/api/myArtikel';
                $headers = [
                    'Authorization' => 'Bearer ' . $token,
                ];
                $check = $this->httpService->get($url, $headers);

                if ($check->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    $paths = [];
                    foreach ($data['data']['data'] as $artikel) {
                        $datatest = Artikel::find((int)$artikel['id']);

                        if ($datatest->getMedia('images')->isNotEmpty()) {
                            $link = $datatest->getMedia('images')->first()->getUrl();
                            $url_parts = parse_url($link);
                            $paths[$artikel['id']] = $url_parts['path'];
                        } else {
                            $paths[$artikel['id']] = ''; // Contoh menggunakan nilai kosong
                        }
                    }

                    foreach ($data['data']['data'] as &$artikel) {
                        if (isset($paths[$artikel['id']])) {
                            $artikel['image_content'] = $paths[$artikel['id']];
                        } else {
                            $artikel['image_content'] = ''; // Atau nilai default lainnya
                        }
                    }
                    return view('Pages.MyArtikel', ['data' => $data, 'roles' => $data['roles']]);
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ]);
            } else if ($responses->getStatusCode() == 401) {
                return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized | Anda Belum Login'
                ], 401);
            }
        } else {
            return redirect('login');
        }
    }

    public function show_detail_artikel($lang, $ids): View|Response|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $authController = new ApiArtikelController();
            $responses = $authController->show_detail_artikel($lang, $ids);
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ])->get("http://project-real-1.localhost/api/{$lang}/artikel/{$ids}");
    
                if ($response->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    $paths = [];
                    $datatest = Artikel::find((int)$data['data']['id']);
                    if ($datatest->getMedia('images')->isNotEmpty()) {
                        $link = $datatest->getMedia('images')->first()->getUrl();
                        $url_parts = parse_url($link);
                        $paths[$data['data']['id']] = $url_parts['path'];
                    } else {
                        $paths[$data['data']['id']] = ''; // Contoh menggunakan nilai kosong
                    }
    
                    if (isset($paths[$data['data']['id']])) {
                        $data['data']['image_content'] = $paths[$data['data']['id']];
                    } else {
                        $data['data']['image_content'] = '';
                    }
                    return view('Pages.ArtikelDetail', ['data' => $data['data'], 'lang' => $lang]);
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ], 401);
            }
        } else {
            return redirect('login');
        }
    }

    public function artikelNonPublication(): View|Response|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $artikelHomeController = new ApiArtikelController();
            $responses = $artikelHomeController->show_artikel_non_publication();
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $url = 'http://project-real-1.localhost/api/artikel-non-publication';
                $headers = [
                    'Authorization' => 'Bearer ' . $token,
                ];
                $check = $this->httpService->get($url, $headers);
                // dd($check);

                if ($check->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    $paths = [];
                    foreach ($data['data']['data'] as $artikel) {
                        $datatest = Artikel::find((int)$artikel['id']);

                        if ($datatest->getMedia('images')->isNotEmpty()) {
                            $link = $datatest->getMedia('images')->first()->getUrl();
                            $url_parts = parse_url($link);
                            $paths[$artikel['id']] = $url_parts['path'];
                        } else {
                            $paths[$artikel['id']] = ''; // Contoh menggunakan nilai kosong
                        }
                    }

                    foreach ($data['data']['data'] as &$artikel) {
                        if (isset($paths[$artikel['id']])) {
                            $artikel['image_content'] = $paths[$artikel['id']];
                        } else {
                            $artikel['image_content'] = ''; // Atau nilai default lainnya
                        }
                    }
                    // dd($data['roles']);
                    return view('Pages.ArtikelNonPublication', ['data' => $data, 'roles' => $data['roles']]);
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ]);
            } else if ($responses->getStatusCode() == 401) {
                return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized | Anda Belum Login'
                ], 401);
            }
        } else {
            return redirect('login');
        }
    }

    public function artikelPublication(): View|Response|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $artikelHomeController = new ApiArtikelController();
            $responses = $artikelHomeController->show_artikel_publication();
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $url = 'http://project-real-1.localhost/api/artikel-publication';
                $headers = [
                    'Authorization' => 'Bearer ' . $token,
                ];
                $check = $this->httpService->get($url, $headers);
                // dd($check);

                if ($check->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    $paths = [];
                    foreach ($data['data']['data'] as $artikel) {
                        $datatest = Artikel::find((int)$artikel['id']);

                        if ($datatest->getMedia('images')->isNotEmpty()) {
                            $link = $datatest->getMedia('images')->first()->getUrl();
                            $url_parts = parse_url($link);
                            $paths[$artikel['id']] = $url_parts['path'];
                        } else {
                            $paths[$artikel['id']] = ''; // Contoh menggunakan nilai kosong
                        }
                    }

                    foreach ($data['data']['data'] as &$artikel) {
                        if (isset($paths[$artikel['id']])) {
                            $artikel['image_content'] = $paths[$artikel['id']];
                        } else {
                            $artikel['image_content'] = ''; // Atau nilai default lainnya
                        }
                    }
                    // dd($data['roles']);
                    return view('Pages.ArtikelPublication', ['data' => $data, 'roles' => $data['roles']]);
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ]);
            } else if ($responses->getStatusCode() == 401) {
                return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized | Anda Belum Login'
                ], 401);
            }
        } else {
            return redirect('login');
        }
    }

    public function artikelCreate(): View|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            return view('Pages.ArtikelCreate');
        } else {
            return redirect('login');
        }
    }

    public function artikelStore(Request $request): RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $bearerNon = session('bearer_token_non_encrypt');
            [$id, $token] = explode('|', $bearerNon, 2);
            $accessToken = PersonalAccessToken::find($id);

            if (hash_equals($accessToken->token, hash('sha256', $token))) {
                $authController = new ApiArtikelController();
                $responses = $authController->store($request);
                if ($responses->status() == 200) {
                    Session::flash('message_input', "Data Artikel {$request->title} Berhasil dibuat");
                    return redirect('artikel');
                } else {
                    return redirect('login');
                }
            }
        }
    }

    public function artikelDestroy($ids): RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $bearerNon = session('bearer_token_non_encrypt');
            [$id, $token] = explode('|', $bearerNon, 2);
            $accessToken = PersonalAccessToken::find($id);
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ])->delete("http://project-real-1.localhost/api/artikel/{$ids}");
            if ($response->status() == 200) {
                return redirect('myArtikel');
            } else {
                return redirect('login');
            }
        }
    }

    public function artikelEdit($ids): View|Response|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $artikelHomeController = new ApiArtikelController();
            $responses = $artikelHomeController->get_artikel_by_id($ids);
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $url = "http://project-real-1.localhost/api/artikel/{$ids}";
                $headers = [
                    'Authorization' => 'Bearer ' . $token,
                ];
                $check = $this->httpService->get($url, $headers);

                if ($check->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    $paths = [];
                    $datatest = Artikel::find((int)$data['dataEn']['id']);
                    if ($datatest->getMedia('images')->isNotEmpty()) {
                        $link = $datatest->getMedia('images')->first()->getUrl();
                        $url_parts = parse_url($link);
                        $paths[$data['dataEn']['id']] = $url_parts['path'];
                    } else {
                        $paths[$data['dataEn']['id']] = ''; // Contoh menggunakan nilai kosong
                    }

                    if (isset($paths[$data['dataEn']['id']])) {
                        $data['dataEn']['image_content'] = $paths[$data['dataEn']['id']];
                    } else {
                        $data['dataEn']['image_content'] = '';
                    }
                    $data['dataEn']['slugId'] = $data['dataId']['slug'];

                    return view('Pages.ArtikelEdit', ['data' => $data['dataEn']]);
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ]);
            } else if ($responses->getStatusCode() == 401) {
                return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized | Anda Belum Login'
                ], 401);
            }
        } else {
            return redirect('login');
        }
    }

    public function artikelUpdate (Request $request): RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $bearerNon = session('bearer_token_non_encrypt');
            [$id, $token] = explode('|', $bearerNon, 2);
            $accessToken = PersonalAccessToken::find($id);

            if (hash_equals($accessToken->token, hash('sha256', $token))) {
                $authController = new ApiArtikelController();
                $responses = $authController->update($request);
                if ($responses->status() == 200) {
                    Session::flash('message_update', "Data Artikel {$request->title} Berhasil diupdate");
                    return redirect('myArtikel');
                } else {
                    return redirect('login');
                }
            }
        } else {
            return redirect('login');
        }
    }

    public function import_excel(): View|RedirectResponse
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            return view('Pages.ArtikelImport');
        } else {
            return redirect('login');
        }
    }

    public function imported_excel(Request $request): Response|RedirectResponse
    {
        // Misalnya, kita memiliki data file yang ingin dikirim dari Code 2 ke Code 1
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $bearerNon = session('bearer_token_non_encrypt');
            [$id, $token] = explode('|', $bearerNon, 2);
            $accessToken = PersonalAccessToken::find($id);

            if (hash_equals($accessToken->token, hash('sha256', $token))) {
                $artikelController = new ApiArtikelController();
                $responses = $artikelController->import_excel();
                if($responses->status() == 200)
                {
                    Session::flash('message_success_import', "Import Success");
                    return redirect('myArtikel');
                } else
                {
                    Session::flash('message_failed_import', "Import Gagal");
                    return redirect('myArtikel');
                }
            }
        } else {
            return redirect('login');
        }
        
    }

    public function export_excel()
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $userId = Auth::user()->id;
            $artikelController = new ApiArtikelController();
            $responses = $artikelController->export_excel($userId);
            if ($responses->getStatusCode() == 200) {
                $bearerNon = session('bearer_token_non_encrypt');
                [$id, $token] = explode('|', $bearerNon, 2);
                $url = 'http://project-real-1.localhost/api/artikel/exports';
                $headers = [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ];
                $check = $this->httpService->get($url, $headers);
    
                if ($check->status() == 200) {
                    $data = json_decode($responses->getContent(), true);
                    if ($data['roles'][0] == 'user') {
                        return Excel::download(new ExportArtikel(false, $userId), 'Exports_' . Carbon::now() . '_Data_Artikel.xlsx');
                    } else {
                        return Excel::download(new ExportArtikel(true, null), 'Exports_' . Carbon::now() . '_Data_Artikel.xlsx');
                    }
    
                    return redirect('/myArtikel');
    
                } else return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized'
                ]);
            }
        } else {
            return redirect('login');
        }
    }
}
