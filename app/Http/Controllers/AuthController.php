<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ArtikelController;
use App\Http\Controllers\Api\AuthController as ApiAuthController;
use App\Models\Artikel;
use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;
use App\Services\HttpService;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\Permission\Models\Role as RoleSpatie;
use Spatie\Permission\Models\Permission;

class AuthController extends Controller
{
    protected $httpService;

    public function __construct(HttpService $httpService)
    {
        $this->httpService = $httpService;
    }

    public function login()
    {
        return view('Auth.Login');
    }

    public function signedIn(Request $request)
    {
        $authController = new ApiAuthController();

        // Panggil fungsi registerUser dari AuthController
        $response = $authController->loginUser($request);
        if ($response->getStatusCode() == 200) {
            [$id, $token] = explode('|', $response->getData()->token, 2);

            $accessToken = PersonalAccessToken::find($id);
            session(['bearer_token_non_encrypt' => $response->getData()->token]);
            if (hash_equals($accessToken->token, hash('sha256', $token))) {
                session(['bearer_token' => $token]);
                if ($accessToken->tokenable->getRoleNames()[0] == 'user') {
                    $artikelHomeController = new ArtikelController();
                    $responses = $artikelHomeController->home();
                    if ($responses->getStatusCode() == 200) {
                        $url = 'http://project-real-1.localhost/api/home';
                        $headers = [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Authorization' => 'Bearer ' . $token,
                        ];
                        $check = $this->httpService->get($url, $headers);

                        if ($check->status() == 200) {
                            return redirect('/');
                        } else return response()->json([
                            'status' => false,
                            'message' => 'Unauthorized'
                        ]);
                    } else if ($responses->getStatusCode() == 401) {
                        return response()->json([
                            'status' => false,
                            'message' => 'Unauthorized | Anda Belum Login'
                        ], 401);
                    }
                } else {
                    return redirect('/');
                }
            }
        } else {
            $data = json_decode($response->getContent(), true);
            Session::flash('login_failed', $data['message']);
            return redirect('/login');
        }

        // dd($response);
    }

    public function register()
    {
        return view('Auth.Register');
    }

    public function signup(Request $request)
    {
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ])->post('http://project-real-1.localhost/api/registerUser', [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
        ]);

        if ($response->status() == 200) {
            return redirect('/login');
        } else {
            if ($response->status() == 401) {
                $dataErr = $response->json()['data']['email'][0];
                Session::flash('email_error', $dataErr);
                return view('Auth.Register', ['error' => $dataErr]);
            }
        }
    }

    public function logoutUser(Request $request)
    {
        if (session('bearer_token_non_encrypt') && session('bearer_token')) {
            $authController = new ApiAuthController();
            $responses = $authController->logoutUser($request);
            if ($responses->status() == 200) {
                $data = json_decode($responses->getContent(), true);
                $user = $request->user();
                $user->tokens()->delete();
                $request->session()->forget('bearer_token');
                $request->session()->forget('bearer_token_non_encrypt');
                Session::flash('logout', $data['message']);
                return redirect('/login');
            }
        } else {
            return redirect('/login');
        }
    }
}
