<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\Permission\Models\Role as RoleSpatie;
use Spatie\Permission\Models\Permission;

class AuthController extends Controller
{
    public function registerUser(Request $request)
    {
        $dataUser = new User();
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
            
            if ($errors->has('email') && $errors->get('email')[0] == 'The email has already been taken.') {
                return response()->json([
                    'status' => false,
                    'message' => 'Email sudah terdaftar',
                ], 401);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Proses validasi gagal',
                    'data' => $errors
                ], 401);
            }
        }

        $dataUser->name = $request->name;
        $dataUser->email = $request->email;
        $dataUser->password = Hash::make($request->password);
        $dataUser->save();

        // $role = RoleSpatie::findByName('user');
        $dataUser->assignRole('user');
        $dataUser->givePermissionTo('artikel-show');
        $dataUser->givePermissionTo('artikel-store');
        $dataUser->givePermissionTo('artikel-edit');
        $dataUser->givePermissionTo('artikel-delete');
        $dataUser->givePermissionTo('artikel-export');

        $roleIdsShow = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-show')
            ->pluck('id')
            ->first();

        $roleIdsStore = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-store')
            ->pluck('id')
            ->first();

        $roleIdsEdit = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-edit')
            ->pluck('id')
            ->first();

        $roleIdsDelete = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-delete')
            ->pluck('id')
            ->first();

        // Gabungkan hasil kueri ke dalam satu array
        $allRoleIds = array_merge([$roleIdsShow], [$roleIdsStore], [$roleIdsEdit], [$roleIdsDelete]);

        // Hapus nilai null dari array jika diperlukan
        $allRoleIds = array_filter($allRoleIds);

        // Ubah array menjadi array numerik
        $allRoleIds = array_values($allRoleIds);

        foreach ($allRoleIds as $roleId) {
            UserRole::create([
                'user_id' => $dataUser->id,
                'abilities_id' => $roleId,
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Berhasil memasukkan data baru',
        ], 200);
    }

    public function loginUser(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Proses login gagal',
                'data' => $validator->errors()
            ], 401);
        }

        if (!Auth::attempt($request->only(['email', 'password']))) 
        {
            return response()->json([
                'status' => false,
                'message' => 'email dan password yang dimasukkan tidak sesuai'
            ], 401);
        }

        // $datauser = User::where('email', $request->email)->first();
        $datauser = QueryBuilder::for(User::class)
                    ->where('email', $request->email)
                    ->first();

        $role = QueryBuilder::for(Role::class)
                ->join('user_role', 'user_role.abilities_id', '=', 'abilities.id')
                ->join('users', 'users.id', '=', 'user_role.user_id')
                ->where('user_id', $datauser->id)
                ->pluck('abilities.role_name')
                ->toArray();

        if(empty($role)) {
            $role = ["*"];
        }
          
        $datauser->tokens()->delete();
        $expiryTime = Carbon::now()->endOfDay();

        activity()
        ->event('signed_in')
        ->log($datauser->name . ' telah login' . ' akses email' . $datauser->email);
        return response()->json([
            'status' => true,
            'message' => 'berhasil proses login',
            'token' => $datauser->createToken('auth_token', $role, $expiryTime)->plainTextToken
        ]);
    }

    public function logoutUser(Request $request)
    {
        $user = $request->user();

        // dd($user);
        $user->tokens()->delete();

        return response()->json([
            'status' => 200,
            'message' => 'User logged out successfully'
        ]);
    }
}
