<?php

namespace App\Http\Controllers\Api;

use App\Events\ExportArtikelProcess;
use App\Events\StoreArtikelProcess;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Exports\ExportArtikel;
use App\Imports\ArtikelImport;
use App\Jobs\ArtikelJob;
use App\Jobs\StoreArtikelJob;
use App\Models\Artikel;
use App\Models\ArtikelTranslations;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\NullableType;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\QueryBuilder\QueryBuilder;

class ArtikelController extends Controller
{
    public function home()
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        $response = QueryBuilder::for(Artikel::class)
            ->defaultSort('-tanggal_publikasi')
            ->whereNotNull('tanggal_publikasi')
            ->limit(4)->get();
        return response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'data' => $response
        ], 200);
    }

    public function myArtikel()
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        $userId = auth('sanctum')->user();
        $userSign = User::find($userId->id);
        $roles = $userSign->getRoleNames();

        $isUserAdmin = in_array('admin', $roles->toArray());

        if ($isUserAdmin) {
            $response = QueryBuilder::for(Artikel::class)
                ->defaultSort('-judul')
                ->jsonPaginate(5);
        } else {
            $response = QueryBuilder::for(Artikel::class)
                ->defaultSort('-judul')
                ->where('user_id', $userId->id)
                ->jsonPaginate(4);
        }
        return response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'roles' => $roles[0],
            'data' => $response
        ], 200);
    }

    public function get_artikel_by_id($id)
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        try {
            App::setLocale('en');
            $queryEn = QueryBuilder::for(Artikel::class)
                ->where('id', $id)
                ->firstOrFail()
                ->setAppends(['slug']);

            App::setLocale('id');
            $queryId = QueryBuilder::for(Artikel::class)
                ->where('id', $id)
                ->firstOrFail()
                ->setAppends(['slug']);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'error' => 'Artikel tidak ditemukan'
            ], 404);
        }

        return response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'dataEn' => $queryEn,
            'dataId' => $queryId,
        ], 200);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        $query = QueryBuilder::for(Artikel::class)
            ->defaultSort('-judul');

        if ($request->has('keyword')) {
            $keyword = $request->keyword;
            $query->where(function ($query) use ($keyword) {
                $query->where('sluggable', 'LIKE', '%' . $keyword . '%');
            });
        }

        $data = $query->jsonPaginate(5);

        activity()
            ->event('visit_artikel')
            ->log(auth('sanctum')->user()->name . ' mengakses halaman artikel ');

        return response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'data' => $data
        ], 200);
    }

    public function show_detail_artikel($lang, $id)
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        App::setLocale($lang);
        try {
            $response = QueryBuilder::for(Artikel::class)
                ->where('id', $id)
                ->firstOrFail()
                ->setAppends(['slug'])
                ->toArray();
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'error' => 'Artikel tidak ditemukan'
            ], 404);
        }

        activity()
            ->event('visit_artikel_detail')
            ->log(auth('sanctum')->user()->name . ' mengakses halaman detail artikel ' . $response['judul']);

        return response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'data' => $response
        ], 200);
    }

    public function show_artikel_non_publication()
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        $userId = auth('sanctum')->user();
        $userSign = User::find($userId->id);
        // Periksa peran pengguna
        $roles = $userSign->getRoleNames();

        $isUserAdmin = in_array('admin', $roles->toArray());

        if ($isUserAdmin) {
            $response = QueryBuilder::for(Artikel::class)
                ->defaultSort('-tanggal_publikasi')
                ->whereNull('tanggal_publikasi')
                ->jsonPaginate(4);
        } else {
            $response = QueryBuilder::for(Artikel::class)
                ->defaultSort('-tanggal_publikasi')
                ->whereNull('tanggal_publikasi')
                ->where('user_id', $userId->id)
                ->jsonPaginate(4);
        }

        return response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'roles' => $roles[0],
            'data' => $response
        ], 200);
    }

    public function show_artikel_publication()
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }

        $userId = auth('sanctum')->user();
        $userSign = User::find($userId->id);
        // Periksa peran pengguna
        $roles = $userSign->getRoleNames();

        $isUserAdmin = in_array('admin', $roles->toArray());

        if ($isUserAdmin) {
            $response = QueryBuilder::for(Artikel::class)
                ->defaultSort('-tanggal_publikasi')
                ->whereNotNull('tanggal_publikasi')
                ->jsonPaginate(4);
        } else {
            $response = QueryBuilder::for(Artikel::class)
                ->defaultSort('-tanggal_publikasi')
                ->where('user_id', $userId->id)
                ->whereNotNull('tanggal_publikasi')
                ->jsonPaginate(4);
        }

        return response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'roles' => $roles[0],
            'data' => $response
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        $data = new Artikel();
        $rules = [
            'judul' => 'required',
            'content' => 'required',
            'image_content' => 'required',
            'category' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal memasukkan data',
                'data' => $validator->errors()
            ], 401);
        }

        $user = auth('sanctum')->user();
        $userSign = User::find($user->id);

        $data->judul = $request->judul;
        $data->content = $request->content;
        $data->image_content = $request->image_content;
        $data->category = $request->category;
        $userSign->hasRole('user') ? '' : $data->tanggal_publikasi = now();
        $data->user_id = $user->id;

        if ($request->slugEn && $request->slugId) {
            $data->translateTo('en');
            $data->slug = $request->slugEn;

            $data->translateTo('id');
            $data->slug = $request->slugId;
        } else if (!$request->slugEn || !$request->slugId) {
            return response()->json([
                'status' => false,
                'message' => 'Lengkapi Slug Translations.',
            ], 401);
        }
        $data->save();

        $data->addMedia($request->image_content)
            ->usingFileName(now() . '_images_content_' . $data->id . '.png')
            ->toMediaCollection('images');
        
        // Send Notification
        event(new StoreArtikelProcess($data));  

        return response()->json([
            'status' => true,
            'message' => 'Berhasil memasukkan data',
            'data_id' => $data->id
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        try {
            $user = auth('sanctum')->user();
            $userSign = User::find($user->id);
            $roles = $userSign->getRoleNames();

            $isUserAdmin = in_array('admin', $roles->toArray());
            if ($isUserAdmin || $user->id === $request->id) {
                $editedArtikel = QueryBuilder::for(Artikel::class)
                    ->where('id', (int)$request->id)
                    ->first();
                $editedArtikel->judul = $request->judul;
                $editedArtikel->content = $request->content;
                $editedArtikel->category = $request->category;
                if ($request->image_content) $editedArtikel->image_content = $request->image_content;

                if ($request->slugEn && $request->slugId) {
                    $editedArtikel->translateTo('en');
                    $editedArtikel->slug = $request->slugEn;

                    $editedArtikel->translateTo('id');
                    $editedArtikel->slug = $request->slugId;
                }

                $editedArtikel->save();
                
                if ($request->has('image_content')) {
                    $deleteMedia = QueryBuilder::for(Media::class)
                        ->where('model_id', (int)$editedArtikel->id)
                        ->first();
                    $deleteMedia->delete();
                    
                    $editedArtikel->addMedia($request->image_content)
                        ->usingFileName(now() . '_images_content_' . $editedArtikel->id . '.png')
                        ->toMediaCollection('images');
                }

                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil mengedit data'
                ], 200);
            } else {
                return response()->json(['message' => 'Not authorized to update this article.'], 401);
            }

            // Jika berhasil mengedit data
            return response()->json([
                'status' => true,
                'message' => 'Berhasil mengedit data'
            ], 200);
        } catch (ModelNotFoundException $e) {
            // Jika gagal karena entitas tidak ditemukan
            return response()->json([
                'status' => false,
                'message' => $e->getMessage() ?: 'Gagal mengedit data | Data Not Found'
            ], 404);
        } catch (\Exception $e) {
            // Jika gagal karena alasan lain
            return response()->json([
                'status' => false,
                'message' => $e->getMessage() ?: 'Gagal mengedit data | Internal Server Error'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        try {
            $user = auth('sanctum')->user();
            $userSign = User::find($user->id);
            $roles = $userSign->getRoleNames();

            $artikel = Artikel::findOrFail($id);

            $isUserAdmin = in_array('admin', $roles->toArray());

            if ($isUserAdmin || $user->id === $artikel->user_id) {
                $artikel->delete();

                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil menghapus data | Data Found'
                ], 200);
            } else {
                return response()->json(['message' => 'Not authorized to delete this article.'], 401);
            }
        } catch (ModelNotFoundException $e) {
            // Jika gagal karena entitas tidak ditemukan
            return response()->json([
                'status' => false,
                'message' => $e->getMessage() . 'Gagal menghapus data | Data Not Found'
            ], 404);
        } catch (\Exception $e) {
            // Jika gagal karena alasan lain
            return response()->json([
                'status' => false,
                'message' => $e->getMessage() ?: 'Gagal menghapus data | Internal Server Error'
            ], 500);
        }
    }

    public function accept_artikel($id)
    {
        // dd($id);
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        $artikel = Artikel::find($id);
        if ($artikel && $artikel->tanggal_publikasi === null) {
            $artikel->tanggal_publikasi = now();
            $artikel->save();
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Artikel sudah dipublikasikan sebelumnya',
            ]);
        }
        $artikel->tanggal_publikasi = now();
        $artikel->save();

        return response()->json([
            'status' => true,
            'message' => 'Berhasil mempublikasikan artikel user'
        ], 200);
    }

    public function export_excel(int|NullableType $id = null)
    {
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }
        if ($id) {
            $userSign = User::find($id);
            $roles = $userSign->getRoleNames();
            $isUserAdmin = in_array('admin', $roles->toArray());
            if ($isUserAdmin) {
                return response()->json([
                    'status' => true,
                    'roles' => $roles,
                    'isAdmin' => true,
                ]);
            } else {
                return response()->json([
                    'status' => true,
                    'roles' => $roles,
                    'isAdmin' => false,
                ]);
            }
        } else {
            // JIKA DI AKSES LEWAT API/POSTMAN

            $user = auth('sanctum')->user();
            $userSign = User::find($user->id);

            // Periksa peran pengguna
            $roles = $userSign->getRoleNames();
            $isUserAdmin = in_array('admin', $roles->toArray());
            if ($isUserAdmin) {
                return Excel::download(new ExportArtikel(true, $id), 'Exports_' . Carbon::now() . '_Data_Artikel.xlsx');
            } else {
                return Excel::download(new ExportArtikel(false, $user->id), 'Exports_' . Carbon::now() . '_Data_Artikel.xlsx');
            }
        }
    }

    public function import_excel()
    {
        // $importedFile ? dd('adass') : dd('tidak adass');
        // request()->file('import_file') ? dd('ada') : dd('tidak ada');
        if (!auth('sanctum')->check()) {
            return response()->json([
                'status' => false,
                'message' => 'Anda belum login.',
            ], 401);
        }

        Excel::import(new ArtikelImport, request()->file('import_file'));

        return response()->json([
            'status' => true,
            'message' => 'success import datas'
        ], 200);
    }
}
