<?php

namespace App\Jobs;

use App\Mail\StoreProcessedMail;
use App\Models\Artikel;
use App\Models\User;
use App\Notifications\StoreProcessedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class StoreArtikelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $artikel;
    /**
     * Create a new job instance.
     */
    public function __construct(Artikel $artikel)
    {
        $this->artikel = $artikel;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Log::info($this->artikel);
        $user = User::find($this->artikel->user_id);
        Mail::to($user->email)->send(new StoreProcessedMail($this->artikel));
        $user->notify(new StoreProcessedNotification($this->artikel));
    }
}
