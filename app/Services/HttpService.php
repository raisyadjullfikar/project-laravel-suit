<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class HttpService
{
    public function get($url, $headers = [])
    {
        return Http::withHeaders($headers)->get($url);
    }

    public function post($url, $data = [], $headers = [])
    {
        return Http::withHeaders($headers)->post($url, $data);
    }

    public function put($url, $data = [], $headers = [])
    {
        return Http::withHeaders($headers)->put($url, $data)->json();
    }

    public function delete($url, $headers = [])
    {
        return Http::withHeaders($headers)->delete($url)->json();
    }
}