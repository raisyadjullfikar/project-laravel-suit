<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Artikel | A'Blogspots</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="storage/css/heroes.css" rel="stylesheet">
    <style>
        /* Custom CSS */
        .cover-image {
            width: 100%;
            max-width: 64px;
            height: auto;
            object-fit: cover;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand text-light" href="{{ route('home') }}">A's Blogspots</a>
            {{-- @if (Auth::user()->role_id != 1)
            @elseif (Auth::user()->role_id == 1)
                <a class="navbar-brand text-light" href="#">Dashboard Admin</a>
            @endif --}}
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page" href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikel') }}">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('myArtikel') }}">myArtikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikelNonPublication') }}">Artikel Non Publish</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikelPublication') }}">Artikel Publish</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page" href="/bookscat">Books
                            Category</a>
                    </li> --}}
                    {{-- @if (Auth::user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link active text-light" aria-current="page" href="/view-all-account">View All
                                Account</a>
                        </li>
                    @endif --}}
                </ul>
                <span class="navbar-text">
                    <a class="text-decoration-none text-light" href="{{ route('logout') }}">Logout</a>
                </span>
            </div>
        </div>
    </nav>
    @if (session()->has('message_failed_import'))
        <div class="pt-3">
            <div class="alert alert-success">
                {{ session('message_failed_import') }}
            </div>
        </div>
    @endif

    @if (session()->has('message_success_import'))
        <div class="pt-3">
            <div class="alert alert-success">
                {{ session('message_success_import') }}
            </div>
        </div>
    @endif

    @if (session()->has('message_update'))
        <div class="pt-3">
            <div class="alert alert-success">
                {{ session('message_update') }}
            </div>
        </div>
    @endif

    <div class="container my-5">
        <div class="mb-3">
            <a href="{{ route('exportArtikel') }}" class="btn btn-success">Export</a>
            @if ($roles == 'admin')
                <a href="{{ route('importArtikel') }}" class="btn btn-info">Import</a>
            @endif
            {{-- <a href="{{ route('exportArtikel') }}" class="btn btn-success">Export</a> --}}
            {{-- <a href="{{ route('importArtikel') }}" class="btn btn-info">Import</a> --}}
        </div>
        <table class="table table-hover table-dark">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Content</th>
                    <th scope="col">Slug</th>
                    <th scope="col">Image</th>
                    <th scope="col">Category</th>
                    <th scope="col">Tanggal Submit</th>
                    <th scope="col">Tanggal Publikasi</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($data))
                    @foreach ($data['data']['data'] as $index => $artikel)
                        <tr>
                            <th scope="row">{{ $index + 1 }}</th>
                            <td>{{ $artikel['judul'] }}</td>
                            <td>{{ $artikel['content'] }}</td>
                            <td>{{ $artikel['sluggable'] }}</td>
                            <td><img src="{{ $artikel['image_content'] }}" alt="Cover Image" class="cover-image"></td>
                            <td>{{ $artikel['category'] }}</td>
                            <td><span class="badge bg-success">{{ $artikel['tanggal_submit'] }}</span></td>
                            <td>
                                @if (!empty($artikel['tanggal_publikasi']))
                                    <span class="badge bg-primary">{{ $artikel['tanggal_publikasi'] }}</span>
                                @else
                                    <span class="badge bg-danger">Belum Dipublikasi</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('artikelEdit', ['id' => $artikel['id']]) }}" class="btn btn-primary btn-sm m-2">Edit</a>
                                <a href="/{{ 'en' }}/artikel/{{ $artikel['id'] }}"
                                    class="btn btn-info btn-sm m-2">Detail</a>
                                {{-- <button type="submit" class="btn btn-danger btn-sm">Delete</button> --}}
                                {{-- <a href="{{ route('editArtikel', ['id' => $artikel['id']]) }}" class="btn btn-primary btn-sm">Edit</a> --}}
                                <form action="{{ route('artikelDestroy', ['id' => $artikel['id']]) }}" method="POST" style="display: inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm m-2">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">Tidak ada data yang tersedia.</td>
                    </tr>
                @endif
            </tbody>
        </table>

        @if ($data['data']['links'])
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    @foreach ($data['data']['links'] as $item)
                        <li class="page-item"><a class="page-link"
                                href="{{ $item['url'] }}">{!! $item['label'] !!}</a></li>
                    @endforeach
                </ul>
            </nav>
        @endif
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
    </script>
</body>

</html>
