<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Artikel Non Publication | A'Blogspots</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="storage/css/heroes.css" rel="stylesheet">
    <style>
        /* Custom CSS */
        .cover-image {
            width: 100%;
            max-width: 64px;
            height: auto;
            object-fit: cover;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand text-light" href="{{ route('home') }}">A's Blogspots</a>
            {{-- @if (Auth::user()->role_id != 1)
            @elseif (Auth::user()->role_id == 1)
                <a class="navbar-brand text-light" href="#">Dashboard Admin</a>
            @endif --}}
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page" 
                            href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikel') }}">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('myArtikel') }}">myArtikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikelNonPublication') }}">Artikel Non Publikasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikelPublication') }}">Artikel Publikasi</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page" href="/bookscat">Books
                            Category</a>
                    </li> --}}
                    {{-- @if (Auth::user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link active text-light" aria-current="page" href="/view-all-account">View All
                                Account</a>
                        </li>
                    @endif --}}
                </ul>
                <span class="navbar-text">
                    <a class="text-decoration-none text-light" href="{{ route('logout') }}">Logout</a>
                </span>
            </div>
        </div>
    </nav>
    @if (session()->has('message_publication_accepted'))
        <div class="pt-3">
            <div class="alert alert-success">
                {{ session('message_publication_accepted') }}
            </div>
        </div>
    @endif

    <div class="container my-5">
        @if ($data['roles'] == 'admin')
            @include('Pages.component.ui.ArtikelNonPublikasiAdmin')
        @elseif ($data['roles'] == 'user')    
            @include('Pages.component.ui.ArtikelNonPublikasi')
        @endif
        

        @if ($data['data']['links'])
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    @foreach ($data['data']['links'] as $item)
                        <li class="page-item"><a class="page-link"
                                href="{{ $item['url'] }}">{!! $item['label'] !!}</a></li>
                    @endforeach
                </ul>
            </nav>
        @endif
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
    </script>
</body>

</html>
