<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home | A'Blogspots</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="storage/css/heroes.css" rel="stylesheet">
    <style>
        .cover-image {
            width: 100%;
            height: 100%;
            max-height: 218px;
            object-fit: cover;
        }

        .view-contain {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        .book-container {
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            overflow: hidden;
            max-width: 800px;
            width: 100%;
            display: grid;
            grid-template-columns: 1fr 1fr;
        }

        .book-cover {
            max-width: 100%;
            height: auto;
        }

        .book-details {
            padding: 20px;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 10px;
            color: #333;
        }

        p {
            font-size: 16px;
            margin-bottom: 10px;
            color: #555;
        }

        .pdf-iframe {
            width: 100%;
            height: 400px;
            border: none;
            margin-bottom: 20px;
            grid-column: span 2;
        }

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .bi {
            vertical-align: -.125em;
            fill: currentColor;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }
    </style>
</head>

<body>
    <div class="px-4 pt-5 my-5 text-center border-bottom">
        <h1 class="display-4 fw-bold"><a href="/artikel" class="text-decoration-none">A'Blogspots</a></h1>
        <div class="col-lg-6 mx-auto">
            <p class="lead mb-4">
                Tempat Artikel seperti blogspots dan mereka bisa mengekspresikan apapun disini. Ayo buat artikel dan
                bergabung bersama kami. Lorem ipsum dolor sit amet, tara tara tak dung tarak tak tak tak dung tak tak
                tak.
            </p>

        </div>
        <div class="overflow-hidden" style="max-height: 30vh;">
            <div class="container px-5">
                <img src="storage/images/bootstrap-docs.png" class="img-fluid border rounded-3 shadow-lg mb-4"
                    alt="Example image" width="700" height="500" loading="lazy">
            </div>
        </div>
    </div>

    <div class="b-example-divider mb-5"></div>
    <div class="container">
        <h2 class="mb-3">Berita Terbaru Disini</h2>
        <!-- START FORM -->
        <div class="row mb-2">
            {{-- {{ $data }} --}}
            @if (!empty($data))
                @foreach ($data as $artikel)
                    <div class="col-md-6">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row g-0">
                                <div class="col-md-4">
                                    <img src="{{ $artikel['image_content'] }}" class="img-fluid rounded-start cover-image" alt="...">
                                    {{-- <img src="/media/uploads/43/2024-03-03-17:39:16_images_content_43.png" class="img-fluid rounded-start cover-image" alt="..."> --}}
                                        
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <strong
                                            class="d-inline-block mb-2 text-primary-emphasis">{{ $artikel['category'] }}</strong>
                                        <h3 class="mb-0">{{ Str::limit($artikel['judul'], 16, '...') }}</h3>
                                        @php
                                            $tanggal_publikasi = \Carbon\Carbon::parse($artikel['tanggal_publikasi']);
                                        @endphp
                                        <p class="card-text fs-6"><small
                                                class="text-body-secondary">{{ $tanggal_publikasi->diffForHumans() }}</small>
                                        </p>
                                        <p class="card-text">{{ Str::limit($artikel['content'], 24, '...') }} <a href=""
                                                class="card-text text-decoration-none">View More...</a></p> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                        <h3>{{ $artikel['judul'] }}</h3>
                        <p>{{ $artikel['content'] }}</p> --}}
                        {{-- Tambahkan kode HTML lainnya sesuai kebutuhan --}}
                    {{-- </div> --}}
                @endforeach
            @else
            <p>Tidak ada data yang tersedia.</p>
        @endif
            {{-- @foreach ($datasArtikel as $key => $data) --}}
                {{-- <div class="col-md-6">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4"> --}}
                                {{-- <img src="storage/{{ $data->image_content }}" --}}
                                    {{-- class="img-fluid rounded-start cover-image" alt="..."> --}}
                            {{-- </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <p>halo 1</p>
                                    <strong --}}
                                        {{-- class="d-inline-block mb-2 text-primary-emphasis">{{ $data->category }}</strong>
                                    <h3 class="mb-0">{{ Str::limit($data->judul, 16, '...') }}</h3>
                                    @php
                                        $tanggal_submit = \Carbon\Carbon::parse($data->tanggal_submit);
                                    @endphp
                                    <p class="card-text fs-6"><small
                                            class="text-body-secondary">{{ $tanggal_submit->diffForHumans() }}</small>
                                    </p>
                                    <p class="card-text">{{ Str::limit($data->content, 24, '...') }} <a href=""
                                            class="card-text text-decoration-none">View More...</a></p> --}}
                                {{-- </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            {{-- @endforeach --}}
        {{-- </div> --}}

        <!-- AKHIR FORM -->
    </div>


    <div class="my-5"></div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
    </script>
</body>

</html>
