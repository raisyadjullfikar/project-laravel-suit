<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article Detail</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <style>
        /* Custom CSS for article detail */
        body {
            padding: 20px;
        }
        .article-container {
            max-width: 50%;
            margin: 0 auto;
        }
        .article-image {
            max-width: 100%;
            height: auto;
            border-radius: 8px;
            margin-bottom: 20px;
        }
        .article-title {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 10px;
        }
        .article-category {
            font-style: italic;
            margin-bottom: 10px;
        }
        .article-content {
            margin-bottom: 20px;
        }
        .article-sluggable {
            font-size: 14px;
            color: #666;
            margin-bottom: 20px;
        }
        .article-meta {
            font-style: italic;
            color: #666;
            margin-bottom: 20px;
        }
        .article-hashtags {
            margin-bottom: 20px;
        }
        .hashtag {
            display: inline-block;
            background-color: #f0f0f0;
            color: #333;
            padding: 5px 10px;
            border-radius: 20px;
            margin-right: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-6">
                <a href="/artikel" class="btn btn-primary">Back</a>
            </div>
            <div class="col-md-2">
                <div class="dropdown">
                    <button class="btn btn-danger dropdown-toggle" type="button" id="languageDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        Lang
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="languageDropdown">
                        <li><a class="dropdown-item {{ $lang === 'en' ? 'active' : '' }}" href="{{ route('showDetailArtikel', ['lang' => 'en', 'id' => $data['id']]) }}">En</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item {{ $lang === 'id' ? 'active' : '' }}" href="{{ route('showDetailArtikel', ['lang' => 'id', 'id' => $data['id']]) }}">Id</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="article-container">
        <img src="{{ $data['image_content'] }}" alt="Article Image" class="article-image">
        <h1 class="article-title">{{ $data['judul'] }}</h1>
        <p class="article-sluggable">{{ $data['sluggable'] }}</p>
        <p class="article-category">Category: {{ $data['category'] }}</p>
        <div class="article-content">
            <p>{{ $data['content'] }}</p>
        </div>
        <div class="article-meta">
            <p><strong>Submitted:</strong> {{ $data['tanggal_submit'] }}</p>
            <p><strong>Published:</strong> {{ $data['tanggal_publikasi'] }}</p>
        </div>
        <div class="article-hashtags">
            <strong>Tags:</strong>
            <span class="hashtag">{{ $data['slug'] }}</span>
            {{-- @foreach($data['slug'] as $slug)
            @endforeach --}}
        </div>
    </div>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</body>
</html>
