<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Artikel Import | A'Blogspots</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="storage/css/heroes.css" rel="stylesheet">
    <style>
        .cover-image {
            width: 100%;
            height: 100%;
            max-height: 218px;
            object-fit: cover;
        }

        .view-contain {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        .book-container {
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            overflow: hidden;
            max-width: 800px;
            width: 100%;
            display: grid;
            grid-template-columns: 1fr 1fr;
        }

        .book-cover {
            max-width: 100%;
            height: auto;
        }

        .book-details {
            padding: 20px;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 10px;
            color: #333;
        }

        p {
            font-size: 16px;
            margin-bottom: 10px;
            color: #555;
        }

        .pdf-iframe {
            width: 100%;
            height: 400px;
            border: none;
            margin-bottom: 20px;
            grid-column: span 2;
        }

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .bi {
            vertical-align: -.125em;
            fill: currentColor;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand text-light" href="{{ route('home') }}">A's Blogspots</a>
            {{-- @if (Auth::user()->role_id != 1)
            @elseif (Auth::user()->role_id == 1)
                <a class="navbar-brand text-light" href="#">Dashboard Admin</a>
            @endif --}}
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page" href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikel') }}">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('myArtikel') }}">myArtikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikelNonPublication') }}">Artikel Non Publish</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page"
                            href="{{ route('artikelPublication') }}">Artikel Publish</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page" href="/bookscat">Books
                            Category</a>
                    </li> --}}
                    {{-- @if (Auth::user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link active text-light" aria-current="page" href="/view-all-account">View All
                                Account</a>
                        </li>
                    @endif --}}
                </ul>
                <span class="navbar-text">
                    <a class="text-decoration-none text-light" href="{{ route('logout') }}">Logout</a>
                </span>
            </div>
        </div>
    </nav>
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('importedArtikel') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="mb-3">
                <label for="import_file">Import File</label>
                {{-- accept="image/jpg, image/jpeg, image/png" --}}
                <input type="file" class="form-control border-primary" name="import_file" id="import_file">
            </div>

            <div class="mb-3">
                <button class="btn btn-success" type="submit">Import Data</button>
            </div>
        </form>
    </div>


    <div class="my-5"></div>
    {{-- <script>
        function previewImage(input) {
            var preview = document.getElementById('preview');
            var file = input.files[0];

            if (file) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    preview.src = e.target.result;
                };

                reader.readAsDataURL(file);
            }
        }
    </script> --}}


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
    </script>
</body>

</html>
