<table class="table table-hover table-dark">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Judul</th>
            <th scope="col">Content</th>
            <th scope="col">Slug</th>
            <th scope="col">Image</th>
            <th scope="col">Category</th>
            <th scope="col">Tanggal Submit</th>
            <th scope="col">Tanggal Publikasi</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @if (!empty($data))
            @foreach ($data['data']['data'] as $index => $artikel)
                <tr>
                    <th scope="row">{{ $index + 1 }}</th>
                    <td>{{ $artikel['judul'] }}</td>
                    <td>{{ $artikel['content'] }}</td>
                    <td>{{ $artikel['sluggable'] }}</td>
                    <td><img src="{{ $artikel['image_content'] }}" alt="Cover Image" class="cover-image"></td>
                    <td>{{ $artikel['category'] }}</td>
                    <td><span class="badge bg-success">{{ $artikel['tanggal_submit'] }}</span></td>
                    <td>
                        @if (!empty($artikel['tanggal_publikasi']))
                            <span class="badge bg-primary">{{ $artikel['tanggal_publikasi'] }}</span>
                        @else
                            <span class="badge bg-danger">Belum Dipublikasi</span>
                        @endif
                    </td>
                    <td>
                        <form action="{{ route('artikelAccept', ['id' => $artikel['id']]) }}" method="POST"
                            style="display: inline;">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-success btn-sm m-2">Accept</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">Tidak ada data yang tersedia.</td>
            </tr>
        @endif
    </tbody>
</table>
