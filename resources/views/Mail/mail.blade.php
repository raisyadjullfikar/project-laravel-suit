<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $judul }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1, p {
            margin: 0 0 20px;
        }
        ul {
            padding: 0;
            margin: 0;
            list-style-type: none;
        }
        li {
            margin-bottom: 10px;
        }
        .btn {
            display: inline-block;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            padding: 10px 20px;
            border-radius: 5px;
        }
        .btn:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Halo,</h1>
        <p>Terima kasih telah menggunakan layanan kami. Berikut adalah informasi terkait artikel yang Anda kirim:</p>
        <ul>
            <li>Judul Artikel: {{ $judul }}</li>
        </ul>
        <p>Jika Anda memiliki pertanyaan lebih lanjut, jangan ragu untuk menghubungi kami.</p>
        <p>Terima kasih,</p>
        <p>Tim Layanan Pelanggan</p>
    </div>
</body>
</html>
