<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Artikel;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role as RoleSpatie;
use Spatie\Permission\Models\Permission;
use Spatie\QueryBuilder\QueryBuilder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        
        // $faker = \Faker\Factory::create();

        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin.art@gmail.com',
            'password' => Hash::make('admin'),
        ]);

        $role = RoleSpatie::create(['name' => 'admin']);
        RoleSpatie::create(['name' => 'user']);

        Permission::create(['name' => 'artikel-show']);
        Permission::create(['name' => 'artikel-store']);
        Permission::create(['name' => 'artikel-edit']);
        Permission::create(['name' => 'artikel-delete']);
        Permission::create(['name' => 'artikel-accept']);
        Permission::create(['name' => 'artikel-import']);
        Permission::create(['name' => 'artikel-export']);
        // Permission::create(['name' => 'user-delete']);

        // $permissions = Permission::pluck('id','id')->all();

        $roleUser = RoleSpatie::findByName('user');
        $roleUser->givePermissionTo('artikel-show');
        $roleUser->givePermissionTo('artikel-store');
        $roleUser->givePermissionTo('artikel-edit');
        $roleUser->givePermissionTo('artikel-delete');
        $roleUser->givePermissionTo('artikel-export');

        $roleUser->syncPermissions('artikel-show');
        $roleUser->syncPermissions('artikel-store');
        $roleUser->syncPermissions('artikel-edit');
        $roleUser->syncPermissions('artikel-delete');
        $roleUser->syncPermissions('artikel-export');
        
        $roleAdmin = RoleSpatie::findByName('admin');
        $roleAdmin->givePermissionTo('artikel-show');
        $roleAdmin->givePermissionTo('artikel-store');
        $roleAdmin->givePermissionTo('artikel-edit');
        $roleAdmin->givePermissionTo('artikel-delete');
        $roleAdmin->givePermissionTo('artikel-accept');
        $roleAdmin->givePermissionTo('artikel-import');
        $roleAdmin->givePermissionTo('artikel-export');

        // BUAT ADMIN
        $role->syncPermissions('artikel-show');
        $role->syncPermissions('artikel-store');
        $role->syncPermissions('artikel-edit');
        $role->syncPermissions('artikel-delete');
        $role->syncPermissions('artikel-accept');
        $role->syncPermissions('artikel-import');
        $role->syncPermissions('artikel-export');
        // $role->syncPermissions('user-delete');
        
        // $user->assignRole([$role->id]);

        // for ($i = 0; $i < 10; $i++) {
        //     Artikel::create([
        //         'judul' => $faker->sentence,
        //         'content' => $faker->paragraph,
        //         'image_content' => $faker->imageUrl(),
        //         'category' => $faker->word,
        //         'tanggal_submit' => now(),
        //         'user_id' => 1, // Ganti dengan logika untuk mendapatkan user_id yang valid
        //     ]);
        // }

        Role::create([
            'role_name' => 'artikel-store'
        ]);

        Role::create([
            'role_name' => 'artikel-show'
        ]);

        Role::create([
            'role_name' => 'artikel-edit'
        ]);

        Role::create([
            'role_name' => 'artikel-delete'
        ]);

        Role::create([
            'role_name' => 'artikel-accept'
        ]);

        // Role::create([
        //     'role_name' => 'user-delete'
        // ]);

        Role::create([
            'role_name' => 'artikel-import'
        ]);

        Role::create([
            'role_name' => 'artikel-export'
        ]);

        $user->assignRole('admin');
        $user->givePermissionTo('artikel-show');
        $user->givePermissionTo('artikel-store');
        $user->givePermissionTo('artikel-edit');
        $user->givePermissionTo('artikel-delete');
        $user->givePermissionTo('artikel-accept');
        $user->givePermissionTo('artikel-import');
        $user->givePermissionTo('artikel-export');
        // $user->givePermissionTo('user-delete');

        $roleIdsShow = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-show')
            ->pluck('id')
            ->first();

        $roleIdsStore = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-store')
            ->pluck('id')
            ->first();

        $roleIdsEdit = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-edit')
            ->pluck('id')
            ->first();

        $roleIdsDelete = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-delete')
            ->pluck('id')
            ->first();

        $roleIdsAccept = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-accept')
            ->pluck('id')
            ->first();

        $roleIdsImport = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-import')
            ->pluck('id')
            ->first();
        $roleIdsExport = QueryBuilder::for(Role::class)
            ->where('role_name', 'artikel-export')
            ->pluck('id')
            ->first();

        // Gabungkan hasil kueri ke dalam satu array
        $allRoleIds = array_merge([$roleIdsShow], [$roleIdsStore], [$roleIdsEdit], [$roleIdsDelete], [$roleIdsAccept], [$roleIdsImport], [$roleIdsExport]);

        // Hapus nilai null dari array jika diperlukan
        $allRoleIds = array_filter($allRoleIds);

        // Ubah array menjadi array numerik
        $allRoleIds = array_values($allRoleIds);

        foreach ($allRoleIds as $roleId) {
            UserRole::create([
                'user_id' => $user->id,
                'abilities_id' => $roleId,
            ]);
        }

        // \App\Models\Artikel::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
