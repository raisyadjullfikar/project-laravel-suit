<?php

use App\Models\Artikel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('artikel', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->text('content');
            $table->string('image_content');
            $table->string('category');
            $table->dateTime('tanggal_submit')->default(now());
            $table->dateTime('tanggal_publikasi')->nullable();
            $table->unsignedBigInteger('user_id'); // Menggunakan tipe data unsignedBigInteger untuk foreign key
            $table->foreign('user_id')->references('id')->on('users'); // Definisi foreign key
            $table->timestamps();
        });

        // $faker = \Faker\Factory::create();
        // for ($i = 0; $i < 10; $i++) {
        //     Artikel::create([
        //         'judul' => $faker->sentence,
        //         'content' => $faker->paragraph,
        //         'image_content' => $faker->imageUrl(),
        //         'category' => $faker->word,
        //         'tanggal_submit' => now(),
        //         'user_id' => 1, // Ganti dengan logika untuk mendapatkan user_id yang valid
        //     ]);
        // }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('artikel');
    }
};
