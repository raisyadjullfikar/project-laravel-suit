<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('artikel_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('artikel_id'); // Menggunakan tipe data unsignedBigInteger untuk foreign key
            $table->foreign('artikel_id')->references('id')->on('artikel')
            ->onDelete('cascade');
            $table->string('locale', 8);
            $table->string('slug')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('artikel_translations');
    }
};
