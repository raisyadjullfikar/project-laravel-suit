<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\AuthController;
use App\Jobs\ArtikelJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Laravel\Sanctum\PersonalAccessToken;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [ArtikelController::class, 'home'])->name('home');
Route::get('/artikel', [ArtikelController::class, 'index'])->name('artikel');
Route::get('/myArtikel', [ArtikelController::class, 'myArtikel'])->name('myArtikel');

Route::get('/login', [AuthController::class, 'login'])->name('signin');
Route::post('/login', [AuthController::class, 'signedIn'])->name('signed');
Route::get('/register', [AuthController::class, 'register'])->name('signup');
Route::post('/register', [AuthController::class, 'signup'])->name('signuped');
Route::get('/logout', [AuthController::class, 'logoutUser'])->name('logout');
Route::get('/artikel/exports', [ArtikelController::class, 'export_excel'])->name('exportArtikel');
Route::get('/artikel/imports', [ArtikelController::class, 'import_excel'])->name('importArtikel');
Route::post('/artikel/imports', [ArtikelController::class, 'imported_excel'])->name('importedArtikel');
Route::get('/artikel/create', [ArtikelController::class, 'artikelCreate'])->name('artikelCreate');
Route::post('/artikel/store', [ArtikelController::class, 'artikelStore'])->name('artikelStore');
Route::get('/artikel/edit/{id}', [ArtikelController::class, 'artikelEdit'])->name('artikelEdit');
Route::put('/artikel/update/{id}', [ArtikelController::class, 'artikelUpdate'])->name('artikelUpdate');
Route::delete('artikel/delete/{id}', [ArtikelController::class, 'artikelDestroy'])->name('artikelDestroy');
Route::get('/artikel-non-publication', [ArtikelController::class, 'artikelNonPublication'])->name('artikelNonPublication');
Route::get('/artikel-publication', [ArtikelController::class, 'artikelPublication'])->name('artikelPublication');

Route::put('/artikel-accept/{id}', [ArtikelController::class, 'accept_artikel'])->name('artikelAccept');
Route::get('/{lang}/artikel/{id}', [ArtikelController::class, 'show_detail_artikel'])->name('showDetailArtikel');

