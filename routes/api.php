<?php

use App\Http\Controllers\Api\AdminController;
use App\Http\Controllers\Api\ArtikelController as ApiArtikelController;
use App\Http\Controllers\Api\AuthController as ApiAuthController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', function() {
    return response()->json([
        'status' => false,
        'message' => 'Akses tidak diperbolehkan',
    ], 401);
})->name('login');

Route::get('/home', [ApiArtikelController::class, 'home']);

Route::get('artikel', [ApiArtikelController::class, 'index'])->middleware(['auth:sanctum', 'permission:artikel-show'], 'ability:artikel-show');
Route::get('myArtikel', [ApiArtikelController::class, 'myArtikel'])->middleware(['auth:sanctum', 'permission:artikel-show'], 'ability:artikel-show');
Route::get('artikel-non-publication', [ApiArtikelController::class, 'show_artikel_non_publication'])->middleware(['auth:sanctum', 'permission:artikel-show'], 'ability:artikel-show');
Route::get('artikel-publication', [ApiArtikelController::class, 'show_artikel_publication'])->middleware(['auth:sanctum', 'permission:artikel-show'], 'ability:artikel-show');
Route::get('{lang}/artikel/{id}', [ApiArtikelController::class, 'show_detail_artikel'])->middleware(['auth:sanctum', 'permission:artikel-show'], 'ability:artikel-show');

// BIKIN ROLE ARTIKEL EXPORT
Route::get('artikel/exports', [ApiArtikelController::class, 'export_excel'])->middleware(['auth:sanctum', 'permission:artikel-export'], 'ability:artikel-export');
Route::post('artikel/imports', [ApiArtikelController::class, 'import_excel'])->middleware(['auth:sanctum', 'permission:artikel-import'], 'ability:artikel-import');

Route::post('artikel', [ApiArtikelController::class, 'store'])->middleware(['auth:sanctum', 'permission:artikel-store'], 'ability:artikel-store');
Route::get('artikel/{id}', [ApiArtikelController::class, 'get_artikel_by_id'])->middleware(['auth:sanctum', 'permission:artikel-show'], 'ability:artikel-show');
Route::delete('artikel/{id}', [ApiArtikelController::class, 'destroy'])->middleware(['auth:sanctum', 'permission:artikel-delete'], 'ability:artikel-delete');
Route::put('artikel', [ApiArtikelController::class, 'update'])->middleware(['auth:sanctum', 'permission:artikel-edit'], 'ability:artikel-edit');
Route::put('artikel-accept/{id}', [ApiArtikelController::class, 'accept_artikel'])->middleware(['auth:sanctum', 'permission:artikel-accept'], 'ability:artikel-accept');

Route::post('registerUser', [ApiAuthController::class, 'registerUser']);
Route::post('loginUser', [ApiAuthController::class, 'loginUser']);
Route::post('logout', [ApiAuthController::class, 'logoutUser'])->middleware(['auth:sanctum']);